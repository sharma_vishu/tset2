package com.exa.Uri;

import java.io.*;

/**
 * Created by vishunath on 25/9/16.
 */
public class ReadingFile {

    public static void main(String [] args) throws IOException {

        File file = new File("/home/vishunath/Desktop/first.xml");

       FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
       // FileReader fr =new FileReader("/home/vishunath/Desktop/first.xml");

      String data;
       // BufferedInputStream bis = new BufferedInputStream(fis);
        BufferedReader br= new BufferedReader(isr);
       while((data =br.readLine() )!=null){
           System.out.println(data);
       }
    }
}
