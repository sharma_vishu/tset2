package com.exa.Uri;


import java.io.*;

import java.net.MalformedURLException;

import java.net.URL;

public class UriAccess {
    public static void main(String[] args) {
        URL url;
        InputStream is = null;
        DataInputStream ds;


        try {
            url = new URL("http://www.programcreek.com/2011/03/java-write-to-a-file-code-example/");

            is = url.openStream();
            ds=new DataInputStream(new BufferedInputStream(is));

          File file = new File("/home/vishunath/Documents/dmo.txt.zip");
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter ow = new OutputStreamWriter(fos);

           String data;

            while((data=ds.readLine())!=null){

                System.out.println(data);
                ow.write(data);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}




